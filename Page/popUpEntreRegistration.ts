import { expect, type Locator, type Page } from '@playwright/test';

export class PopUpRegAut {
  readonly page: Page;
  readonly fieldEmailAuth: Locator;
  readonly buttonContinue: Locator;
  readonly fieldFIO: Locator;
  readonly fieldEmailReg: Locator;
  readonly fieldPhone: Locator;
  readonly fieldPassReg: Locator;
  readonly fieldPassConfirmReg: Locator;
  readonly checkboxRegistration: Locator;
  readonly buttonRegistration: Locator;
  readonly fieldPassAuth: Locator;
  readonly validationFIO: Locator;
  readonly validationPhone: Locator;
  readonly validationPass: Locator;
  readonly validationPassConfirm: Locator;
  readonly validationCheckBoxRegistration: Locator;
  readonly buttonEntre: Locator;

  constructor(page: Page) {
    this.page = page;
    //Entre or Registration
    this.fieldEmailAuth = page.getByRole('textbox', { name: 'Email' });
    this.buttonContinue = page.getByRole('button', { name: 'Продолжить' });
    this.fieldPassAuth = page.getByRole('textbox', { name: 'Пароль' });
    this.buttonEntre = page.locator('#form-combo-auth').getByRole('button', { name: 'Войти' });
    //Registration
    this.fieldFIO = page.getByPlaceholder('ФИО');
    this.fieldEmailReg = page.getByRole('textbox', { name: 'Email' });
    this.fieldPhone = page.getByPlaceholder('Телефон');
    this.fieldPassReg = page.getByRole('textbox', { name: 'Пароль', exact: true });
    this.fieldPassConfirmReg = page.getByPlaceholder('Подтвердить пароль');
    this.checkboxRegistration = page.locator('input[type="checkbox"]');
    this.buttonRegistration = page.getByRole('button', { name: 'Регистрация' });


    //Text Validation during registration
    this.validationFIO = page.getByText('Введите корректное имя и фамилию');
    this.validationPhone = page.getByText('Номер телефона должен содержать 11 цифр');
    this.validationPass = page.getByText('Введите 8 или более символов');
    this.validationPassConfirm = page.getByText('Пароли не совпадают');
    this.validationCheckBoxRegistration = page.getByText('Дайте согласие на обработку персональных данных');

  }

//   async goto() {
//     await this.page.goto('https://playwright.dev');
//   }

 
}