import { expect, type Locator, type Page } from '@playwright/test';

export class Header {
  readonly page: Page;
  readonly entreForPC: Locator;
  readonly closePreHeader: Locator;
  readonly chooseCityForPC: Locator;
  readonly logo: Locator;
  readonly fieldSearchForPC: Locator;
  readonly favotite: Locator;
  readonly cart: Locator;
  readonly buttonYesForCityPC: Locator;
  readonly buttonNoForCityPC: Locator;
  readonly privetOffice: Locator;
  readonly privateOfficeLC: Locator;
  readonly privateOfficeMyOrder: Locator;
  readonly privateOfficeExit: Locator;

  
  constructor(page: Page) {
    this.page = page;

    this.closePreHeader = page.locator('#closePreHeader');

    this.chooseCityForPC = page.getByRole('button', { name: 'Выберите регион' });

    this.logo = page.getByRole('link', { name: 'ЛУКОИЛ' });
    this.fieldSearchForPC = page.locator('#title-search-input');
    this.entreForPC = page.getByText('Регистрация').first(); //For not authorized 
    this.favotite = page.getByRole('link', { name: '0', exact: true }).first();
    this.cart = page.getByRole('link', { name: '0 cart' });
    this.buttonYesForCityPC = page.getByRole('button', { name: 'Да, все верно' });
    this.buttonNoForCityPC = page.getByRole('button', { name: 'Выбрать другой' });
    // For Auth customer
    this.privetOffice = page.locator("div[class='b-header-mid__container'] div[class='user-actions-name']");
    this.privateOfficeLC = page.getByRole('link', { name: 'Мой профиль' });
    this.privateOfficeMyOrder = page.getByRole('link', { name: 'Мой профиль' });
    this.privateOfficeExit = page.getByRole('link', { name: 'Мой профиль' });
  }

  async goto() {
    // await this.page.route('**/*.{png,jpg,jpeg}', route => route.abort());
    await this.page.goto('https://rc.lukoil-shop.ru/');
  }

 
}