import { expect, test } from '@playwright/test';
import { Header } from '../../Page/header';
import { PopUpRegAut} from '../../Page/popUpEntreRegistration';
// test.beforeEach(async ({ page }, testInfo) => {
//         await page.goto('https://rc.lukoil-shop.ru/');
//       });

test('Registration', async ({ page }) => {
   const header = new Header(page);
   const popUpRegAut = new PopUpRegAut(page);

   await page.goto ('https://rc.lukoil-shop.ru/');
   await header.entreForPC.click();
   await popUpRegAut.fieldEmailAuth.fill('test.lukoil-shop@mail.ru');
   await popUpRegAut.buttonContinue.click();
   await expect (page.getByRole('textbox', { name: 'Email' })).toBeVisible;
   await popUpRegAut.fieldFIO.fill('Name');
   await popUpRegAut.fieldPhone.fill('656656565');
   await popUpRegAut.fieldPassReg.fill('123456');
   await popUpRegAut.fieldPassConfirmReg.fill('123');
   await popUpRegAut.buttonRegistration.click();
   await expect (popUpRegAut.validationFIO).toBeVisible();
   await expect (popUpRegAut.validationPhone).toBeVisible();
   await expect (popUpRegAut.validationPass).toBeVisible();
   await expect (popUpRegAut.validationPassConfirm).toBeVisible();
   await expect (popUpRegAut.validationCheckBoxRegistration).toBeVisible();
   await popUpRegAut.fieldFIO.fill('Name SurName');
   await popUpRegAut.fieldPhone.fill('9685741236');
   await popUpRegAut.fieldPassReg.fill('12345678');
   await popUpRegAut.fieldPassConfirmReg.fill('12345678');
   await popUpRegAut.page.evaluate("$('#agreeCheck').trigger('click')");
   await popUpRegAut.buttonRegistration.click();
   await popUpRegAut.page.waitForResponse(response => response.url() === 'https://rc.lukoil-shop.ru/lapi/shop/auth/register' && response.status() === 200);
  //  await expect(header.privetOffice).toHaveText('Name SurName');
  });

  test('Auth and change personal information', async ({ page }) => {
    const header = new Header(page);
    const popUpRegAut = new PopUpRegAut(page);

    await page.goto ('https://rc.lukoil-shop.ru/');
    await header.privetOffice.click();
    await popUpRegAut.fieldEmailAuth.fill('test.lukoil@mail.ru');
    await popUpRegAut.buttonContinue.click();
    await popUpRegAut.fieldPassAuth.fill('12345678');
    await popUpRegAut.buttonEntre.click();
    // await expect(header.privetOffice).toHaveText('Name SurName');

    await header.privetOffice.hover();
    await header.privateOfficeLC.click();

  });

  test('Auth with new credentionals', async ({ page }) => {
    
  });